# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=vala
pkgver=0.56.5
pkgrel=1
pkgdesc="Compiler for the GObject type system"
url="https://wiki.gnome.org/Projects/Vala"
arch="all"
license="LGPL-2.0-or-later"
subpackages="$pkgname-dbg $pkgname-doc"
depends="glib-dev"
makedepends="libxslt-dev bash flex bison gobject-introspection-dev graphviz-dev"
checkdepends="dbus-x11"
source="https://download.gnome.org/sources/vala/${pkgver%.*}/vala-$pkgver.tar.xz"

prepare() {
	default_prepare
	# 2 failures
	sed -i "/constants\/member-access/d" \
		tests/Makefile.in
}

build() {
	CFLAGS="$CFLAGS -O2 -flto=auto" \
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
58bb5ea45f519414ffe0dad2f23dcf689eac2d8cd310a627d0c3667f38ec4ccd5ec1ca5921f8ec177db3004f24c7c5a138bd4652c7a07a78f0ce046e32ad9b6a  vala-0.56.5.tar.xz
"
