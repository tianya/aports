# Contributor: Olivier Mauras <olivier@mauras.ch>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=borgbackup
_pkgname=borg
pkgver=1.2.4
pkgrel=1
pkgdesc="Deduplicating backup program"
url="https://www.borgbackup.org/"
# armhf: Bus error in tests
arch="all !armhf"
license="BSD-3-Clause"
depends="
	py3-msgpack
	py3-packaging
	python3
	"
makedepends="
	acl-dev
	attr-dev
	linux-headers
	lz4-dev
	openssl-dev>3
	py3-setuptools
	py3-setuptools_scm
	python3-dev
	zstd-dev
	"
checkdepends="
	py3-dateutil
	py3-pytest
	py3-pytest-benchmark
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/borgbackup/borg/releases/download/$pkgver/borgbackup-$pkgver.tar.gz"

export BORG_OPENSSL_PREFIX="/usr/include/openssl"

build() {
	# https://github.com/borgbackup/borg/issues/4891#issuecomment-615838317
	CFLAGS="$CFLAGS -DXXH_FORCE_MEMORY_ACCESS=1" \
	python3 setup.py build
}

check() {
	PYTHONPATH="$(echo "$builddir"/build/lib.linux-*)" pytest -v \
	--benchmark-skip --pyargs borg.testsuite
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	install -Dm644 -t "$pkgdir"/usr/share/man/man1 docs/man/*.1

	install -Dm644 scripts/shell_completions/bash/$_pkgname \
		"$pkgdir"/usr/share/bash-completion/completions/$_pkgname

	install -Dm644 scripts/shell_completions/fish/$_pkgname.fish \
		"$pkgdir"/usr/share/fish/completions/$_pkgname.fish

	install -Dm644 scripts/shell_completions/zsh/_$_pkgname \
		"$pkgdir"/usr/share/zsh/site-functions/_$_pkgname

	# clean some useless files
	cd "$pkgdir"/usr/lib/python*/site-packages/borg
	find . -name '*.h' -delete -o -name '*.c' -delete -o -name '*.pyx' -delete
}

sha512sums="
9326a58605a085f521ba75a2eecfb4bdb790d3da18c77076bddaeae641678cc08171d997a758182a739ec6b145bb04d982f8314ae60f93c3ff093eca15515dfd  borgbackup-1.2.4.tar.gz
"
