# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-chaospy
_pkgorig=chaospy
pkgver=4.3.11
pkgrel=0
pkgdesc="Numerical tool for performing uncertainty quantification"
url="https://github.com/jonathf/chaospy"
arch="noarch !x86 !armhf !armv7 !s390x" # assertion errors
license="MIT"
depends="
	python3
	py3-matplotlib
	py3-numpoly
	py3-numpy
	py3-scipy
	py3-scikit-learn
	"
checkdepends="python3-dev py3-pytest"
makedepends="
	py3-gpep517
	py3-setuptools_scm
	py3-wheel
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/jonathf/chaospy/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgorig-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer --destdir="$pkgdir" dist/*.whl
}

sha512sums="
8929b5d5980f9f9e9bc5c26759744d29aaf02913cb61cbe335b1b49a712f931cdb17cd826ce690d77fb0616c0ec1040586916dd29720031ede933c68b8805892  py3-chaospy-4.3.11.tar.gz
"
