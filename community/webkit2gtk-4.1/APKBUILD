# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Jiri Horner <laeqten@gmail.com>
# Maintainer: knuxify <knuxify@gmail.com>
pkgname=webkit2gtk-4.1
pkgver=2.40.0
pkgrel=1
pkgdesc="Portable web rendering engine WebKit for GTK+ - GTK+3 libsoup3 version"
url="https://webkitgtk.org/"
arch="all"
license="LGPL-2.0-or-later AND BSD-2-Clause"
depends="bubblewrap xdg-dbus-proxy dbus:org.freedesktop.Secrets"
makedepends="
	bison
	clang
	cmake
	enchant2-dev
	flex
	flite-dev
	geoclue-dev
	gnutls-dev
	gobject-introspection-dev
	gperf
	gst-plugins-bad-dev
	gst-plugins-base-dev
	gstreamer-dev
	gtk+3.0-dev
	hyphen-dev
	icu-dev
	lcms2-dev
	libavif-dev
	libgcrypt-dev
	libjpeg-turbo-dev
	libmanette-dev
	libpng-dev
	libseccomp-dev
	libsecret-dev
	libsoup3-dev
	libwebp-dev
	libwpe-dev
	libwpebackend-fdo-dev
	libxml2-dev
	libxslt-dev
	libxt-dev
	llvm
	mesa-dev
	openjpeg-dev
	pango-dev
	python3
	ruby
	samurai
	sqlite-dev
	unifdef
	woff2-dev
	"
replaces="webkit"
options="!check" # upstream doesn't package them in release tarballs: Tools/Scripts/run-gtk-tests: Command not found
subpackages="$pkgname-dbg $pkgname-lang $pkgname-dev"
source="https://webkitgtk.org/releases/webkitgtk-$pkgver.tar.xz
	initial-exec.patch
	riscv-fix-1.patch
	riscv-fix-2.patch
	riscv-fix-3.patch
	riscv-fix-4.patch
	riscv-fix-5.patch
	riscv-fix-6.patch
	riscv-fix-7.patch
	"
builddir="$srcdir/webkitgtk-$pkgver"

case "$CARCH" in
s390x)
	;;
*)
	makedepends="$makedepends lld"
	;;
esac

build() {
	case "$CARCH" in
	s390x|armhf|armv7|x86|ppc64le)
		# llint/LowLevelInterpreter.cpp fails to build with fortify source here
		export CXXFLAGS="$CXXFLAGS -U_FORTIFY_SOURCE"
		;;
	esac

	case "$CARCH" in
	armv7)
		# clang fails to build armv7 due to some NEON related thing.
		# https://github.com/WebKit/WebKit/pull/1233 should fix it
		;;
	s390x)
		# no lld on s390x
		export CC=clang
		export CXX=clang++
		;;
	*)
		# the debug symbols become 1/2 the size, and actual webkit becomes
		# smaller too.
		export CC=clang
		export CXX=clang++
		export LDFLAGS="$LDFLAGS -fuse-ld=lld"
		;;
	esac

	case "$CARCH" in
	arm*|aarch64|s390x|riscv64)
		# arm: seemingly broken?
		# s390x/riscv64: no lld
		;;
	*)
		local lto="-DLTO_MODE=thin"
		;;
	esac

	export AR=llvm-ar
	export NM=llvm-nm
	export RANLIB=llvm-ranlib

	# significantly reduce debug symbol size
	export CFLAGS="$CFLAGS -g1"
	export CXXFLAGS="$CXXFLAGS -g1"

	cmake -B build -G Ninja \
		-DPORT=GTK \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_SKIP_RPATH=ON \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_DOCUMENTATION=OFF \
		-DENABLE_JOURNALD_LOG=OFF \
		-DENABLE_MINIBROWSER=ON \
		-DENABLE_SAMPLING_PROFILER=OFF \
		$lto
	cmake --build build
}

check() {
	ninja -C build check
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	mv "$pkgdir"/usr/bin/WebKitWebDriver "$pkgdir"/usr/bin/WebKitWebDriver-4.1
}

dbg() {
	# hack to use over binutils objcopy for riscv
	CROSS_COMPILE=llvm-
	default_dbg
}

sha512sums="
550dafb31d71edf984d6b7636366f839d31f6b244b87f83c6efe7af17efe3f2f63268d2c39a6a3c474afadf2b30df868efaefcf38197b30cc6b11c63de7d2ddc  webkitgtk-2.40.0.tar.xz
26f3df81758068a83bf770e1f8b48546e9ec2428d23cbc4e1c5cc7851c91ad1dfeeac89aea73568a5f498cd6c053aaab7e1af67e59a471ad2d0375c1c64cbd8a  initial-exec.patch
8ddd2cb2a10aa4c9296ae641e15ff8b58fd48c9fd9ccc5a4b2ebc40c4a90e3f2cffa3d9e030b0038a169d97f463b62d64707caa2c64eaea21cfce0f0a04d29d4  riscv-fix-1.patch
a5a5d62aea820c087f7919b617bef5adefa6f34a8189fe1993250535ff59585bed4c6fd24d69caaeb814dcaa449194dd533886a41e70acceb3882f8399494404  riscv-fix-2.patch
5f9c44fd9b29587110208ad63e845eb178cc517a56e18bd0a4c17acc5dfa772c6b92bacad219b9e1910e030e019b125a7ec523f85af013bd53f05e4e30e595cb  riscv-fix-3.patch
1c33fa822a245f0f8db1caa3b368d3dd56e2595e509fd422d7d888bbe23288426907a4dd4fc0e14a1d28ff3c3240cb69e4ef8a6326ec27eb1db50c31b84da006  riscv-fix-4.patch
ba730685aee231d0941229601e4dedc70c53737c7702d9b58600f9bf4eff793490b218f042750145363902570a0915da720726bd969092b402d03413536db714  riscv-fix-5.patch
e7193564d415c4c71d735e897ce74b6efa49eb43060e8a44a494e854ae67099588bc982bce1a6ced27726589e2f93fc2e1ff8c5bb99b3444cb7d479c31b233e0  riscv-fix-6.patch
15c34e6fc59279627ea6801d8954a95c5d8ba711cfb6ae7aad1335739675940a244137c6b1a055ae6f7c8017da07430d9b1b9a4b789cdc4c9072b585a4573bd1  riscv-fix-7.patch
"
