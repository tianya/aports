# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdevelop
pkgver=22.12.3
pkgrel=1
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://www.kdevelop.org/"
pkgdesc="A featureful, plugin-extensible IDE for C/C++ and other programming languages"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
depends="indent"
makedepends="
	boost-dev
	clang
	clang-dev
	extra-cmake-modules
	grantlee-dev
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kcrash-dev
	kdeclarative-dev
	kdoctools-dev
	kguiaddons-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kitemviews-dev
	kjobwidgets-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kservice-dev
	ktexteditor-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkomparediff2-dev
	libksysguard-dev
	llvm-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	shared-mime-info
	threadweaver-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdevelop-$pkgver.tar.xz
	fix-find-clang-path.patch
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
465bf3639e3fd61058af1217904fe0904d7c380e1f073dcb3af28d5758bba1d477e2f1313c61c233cf2acef4362c5380b30bcae6a02eb704a8a3fdf80ddfd75a  kdevelop-22.12.3.tar.xz
47b271ed48c82c489d62314e30438667baddef2a05bb0c85062fa1e2fdc8681895c4d561f2d2c250da2ed9275a2ef73fad82b3d164f58ed8272cc9ec5190cef2  fix-find-clang-path.patch
"
