# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=ncspot
pkgver=0.13.0
pkgrel=0
pkgdesc="Cross-platform ncurses Spotify client inspired by ncmpc and the likes"
url="https://github.com/hrkfdn/ncspot"
# ppc64le: fails to build ring crate
# others: limited by rust/cargo
arch="aarch64 armhf armv7 x86 x86_64"
license="BSD-2-Clause"
makedepends="
	cargo
	dbus-dev
	libxcb-dev
	ncurses-dev
	openssl-dev>3
	pulseaudio-dev
	python3
	"
source="https://github.com/hrkfdn/ncspot/archive/v$pkgver/ncspot-$pkgver.tar.gz"
options="!check"  # there's only one unit test (in v0.8.1)

# Optimize binary for size (18 MiB -> 8.2 MiB in v0.8.1).

_cargo_opts='--frozen --features cover,share_selection'

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build $_cargo_opts --release
}

check() {
	cargo test $_cargo_opts
}

package() {
	cargo install $_cargo_opts --offline --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*
}

sha512sums="
ea9eee4d4af3a530187c8b0422103cc2d269d8cce7e2365cd87a9ed4c08389b257f5b8b6de5750a0ba3c4657f512cc30effb4774033b711a28eda80111d1fe58  ncspot-0.13.0.tar.gz
"
