# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-pylint
pkgver=2.17.2
pkgrel=0
pkgdesc="Analyzes Python code looking for bugs and signs of poor quality"
url="https://github.com/PyCQA/pylint"
arch="noarch !s390x" # py3-dill
license="GPL-2.0-or-later"
depends="
	py3-astroid
	py3-dill
	py3-isort
	py3-mccabe
	py3-platformdirs
	py3-tomlkit
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-gitpython
	py3-py
	py3-pytest
	py3-pytest-benchmark
	py3-pytest-runner
	py3-pytest-timeout
	py3-pytest-xdist
	py3-requests
	py3-typing-extensions
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/PyCQA/pylint/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir"/pylint-$pkgver

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest --benchmark-disable -v tests
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/pylint-*.whl
}

sha512sums="
9c3063332ffc102ba4a7177cca4c440ce89a8ba6fa4252eb1fa401a6e6c10e49afcbbe76127aedccc30dfe5f0dfe7b19cd9aadf4f5ffaa2494a4743d2e0d6a33  py3-pylint-2.17.2.tar.gz
"
